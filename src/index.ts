import { scope } from './assets/scopes';
import express, { response, urlencoded } from "express"
import cryptoJs from 'crypto-js';
import axios from 'axios';
import dotenv from "dotenv"
import ejs from "ejs"

dotenv.config()

const app = express()

app.engine('html', ejs.renderFile);
app.set('view engine', 'html');
app.set('views', __dirname + `/pages`);
app.set("PORT", 4000)

app.get('/', (req, res) => {
    res.redirect('/install')
})


app.get('/auth', (req, res) => {
    /*
    let params: any = {}
    Object.keys(req.query).forEach((param: string) => {
        if (param !== 'hmac') {
            params[param] = req.query[param]
        }
    })
    params = new URLSearchParams(params).toString()*/
    const secret: string | undefined = process.env.SHOPIFY_SECRET_API_KEY 
    /*let mac;
    if (secret) {
        mac = cryptoJs.HmacSHA256(params, secret ).toString()
    } */
    if (req.query['code']) {
        const shopifyUrl: string | undefined = process.env.SHOPIFY_STORE_URL
        const clientKey: string | undefined = process.env.SHOPIFY_API_KEY
        axios.post(`${shopifyUrl}/admin/oauth/access_token`, {
            client_id: clientKey,
            client_secret: secret,
            code: req.query['code']
        }).then(response => {
            console.log('respuesta data')
            console.log('-------------------------------------------------------------------')
            console.log(response)
        }).catch(err => {
            console.log(err)
        })
    }
    res.render(__dirname + '/pages/installed.html')
})


app.get('/install', (_, res) => {
    const scopes: string = scope.join(',')
    const shopifyUrl: string | undefined = process.env.SHOPIFY_STORE_URL
    const clientKey: string | undefined = process.env.SHOPIFY_API_KEY
    const redirectUrl: string = "http://15cd-2800-484-cb7c-6900-cdb8-6d1b-bf82-b7ca.ngrok.io/auth"
    const url: string = `${shopifyUrl}/admin/oauth/authorize?client_id=${clientKey}&scope=${scopes}&redirect_uri=${encodeURIComponent(redirectUrl)}`
    res.render(__dirname + `/pages/install.html`, {link: url})
})


app.get('/products', async (_, res) => {
    try {
        const shopifyUrl: string | undefined = process.env.SHOPIFY_STORE_URL
        const token: string | undefined = process.env.SHOPIFY_PERMANENT_TOKEN
        if (token) {
            const response = await axios.get(`${shopifyUrl}/admin/api/2021-10/products.json`, {
                headers: {
                    'X-Shopify-Access-Token' : "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"
                }
            })
            console.log(response)
            res.send(response.data)
        }
    } catch (error) {
        console.error(`Error: ${error}`)
        res.send(error)
    }
})

app.get('/orders', async(_, res)=> {
    try {
        const shopifyUrl: string | undefined = process.env.SHOPIFY_STORE_URL
        const token: string | undefined = process.env.SHOPIFY_PERMANENT_TOKEN
        if (token) {
            const response = await axios.get(`${shopifyUrl}/admin/api/2021-10/orders.json`, {
                headers: {
                    'X-Shopify-Access-Token' : "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"
                }
            })
            console.log(response)
            res.send(response.data)
        }
    } catch (error) {
        console.error(`Error: ${error}`)
        res.send(error)
    }
})


app.post('/orders',async (req, res) => {
    try {
        const shopifyUrl: string | undefined = process.env.SHOPIFY_STORE_URL
        const token: string | undefined = process.env.SHOPIFY_PERMANENT_TOKEN
        if (token) {
            const response = await axios.post(`${shopifyUrl}/admin/api/2021-10/orders.json`, req.body , {
                headers: {
                    'X-Shopify-Access-Token' : token
                }
            })
            console.log(response.data)
            res.send(response.data)
        }
    } catch (error) {
        console.error(error)
        res.send(error)
    }
})



const server = app.listen(app.get('PORT'), ()=> {
    console.log("App running on port 4000")
})