"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const scopes_1 = require("./assets/scopes");
const express_1 = __importDefault(require("express"));
const axios_1 = __importDefault(require("axios"));
const dotenv_1 = __importDefault(require("dotenv"));
const ejs_1 = __importDefault(require("ejs"));
dotenv_1.default.config();
const app = (0, express_1.default)();
app.engine('html', ejs_1.default.renderFile);
app.set('view engine', 'html');
app.set('views', __dirname + `/pages`);
app.set("PORT", 4000);
app.get('/', (req, res) => {
    res.redirect('/install');
});
app.get('/auth', (req, res) => {
    /*
    let params: any = {}
    Object.keys(req.query).forEach((param: string) => {
        if (param !== 'hmac') {
            params[param] = req.query[param]
        }
    })
    params = new URLSearchParams(params).toString()*/
    const secret = process.env.SHOPIFY_SECRET_API_KEY;
    /*let mac;
    if (secret) {
        mac = cryptoJs.HmacSHA256(params, secret ).toString()
    } */
    if (req.query['code']) {
        const shopifyUrl = process.env.SHOPIFY_STORE_URL;
        const clientKey = process.env.SHOPIFY_API_KEY;
        axios_1.default.post(`${shopifyUrl}/admin/oauth/access_token`, {
            client_id: clientKey,
            client_secret: secret,
            code: req.query['code']
        }).then(response => {
            console.log('respuesta data');
            console.log('-------------------------------------------------------------------');
            console.log(response);
        }).catch(err => {
            console.log(err);
        });
    }
    res.render(__dirname + '/pages/installed.html');
});
app.get('/install', (_, res) => {
    const scopes = scopes_1.scope.join(',');
    const shopifyUrl = process.env.SHOPIFY_STORE_URL;
    const clientKey = process.env.SHOPIFY_API_KEY;
    const redirectUrl = "http://15cd-2800-484-cb7c-6900-cdb8-6d1b-bf82-b7ca.ngrok.io/auth";
    const url = `${shopifyUrl}/admin/oauth/authorize?client_id=${clientKey}&scope=${scopes}&redirect_uri=${encodeURIComponent(redirectUrl)}`;
    res.render(__dirname + `/pages/install.html`, { link: url });
});
app.get('/products', (_, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const shopifyUrl = process.env.SHOPIFY_STORE_URL;
        const token = process.env.SHOPIFY_PERMANENT_TOKEN;
        if (token) {
            const response = yield axios_1.default.get(`${shopifyUrl}/admin/api/2021-10/products.json`, {
                headers: {
                    'X-Shopify-Access-Token': "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"
                }
            });
            console.log(response);
            res.send(response.data);
        }
    }
    catch (error) {
        console.error(`Error: ${error}`);
        res.send(error);
    }
}));
app.get('/orders', (_, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const shopifyUrl = process.env.SHOPIFY_STORE_URL;
        const token = process.env.SHOPIFY_PERMANENT_TOKEN;
        if (token) {
            const response = yield axios_1.default.get(`${shopifyUrl}/admin/api/2021-10/orders.json`, {
                headers: {
                    'X-Shopify-Access-Token': "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"
                }
            });
            console.log(response);
            res.send(response.data);
        }
    }
    catch (error) {
        console.error(`Error: ${error}`);
        res.send(error);
    }
}));
app.post('/orders', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const shopifyUrl = process.env.SHOPIFY_STORE_URL;
        const token = process.env.SHOPIFY_PERMANENT_TOKEN;
        if (token) {
            const response = yield axios_1.default.post(`${shopifyUrl}/admin/api/2021-10/orders.json`, req.body, {
                headers: {
                    'X-Shopify-Access-Token': token
                }
            });
            console.log(response.data);
            res.send(response.data);
        }
    }
    catch (error) {
        console.error(error);
        res.send(error);
    }
}));
const server = app.listen(app.get('PORT'), () => {
    console.log("App running on port 4000");
});
